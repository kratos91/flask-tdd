
## Instalar dependencias
```sh
$ > pip install -r requirements/development.txt
```

## Dar permisos al archivo manage.py
```sh
$ > chmod 775 manage.py
```

## Correr aplicacion con docker
```sh
$ > ./manage.py compose up -d
```


## Detener la aplicacion
```sh
$ > ./manage.py compose down
```

## Reconstruir imagen de docker
```sh
$ > ./manage.py compose build web
```

## Correr tests
```sh
$ > ./manage.py test
```